Programmation d’interfaces Contact Book

1 Decription

On souhaite écrire une application de gestion de contacts téléphoniques avec une interface graphique. Pour chaque personne on sauvegarde son nom et prénom .... Chaque contact peut avoir plusieurs numéros de téléphones. Les informations à sto- cker pour chaque contacte sont les suivantes :
–-
Nom : Doe
Prénom : Jane
Adresse : xx
CP : xxxxx
Type contact : Pro, Perso, Autre
e-mail : xx@xx.xxx
Num_1 : 01 02 03 04 05
Num_2 : 01 02 03 04 06
Num_n : 01 02 03 04 09

2 Fonctionnalités

L’application doit :
— Afficher la liste des contacts
— Le clique sur un contact provoque l’affichage des informations du contact, l’af-
fichage peut se faire dans une boîte de dialogue ou dans une partie de la fenêtre
principale.
— Pour chaque contact on doit pouvoir mettre à jour les informations.
— On doit pouvoir supprimer un contact.
— On doit pouvoir ajouter un contact. Lors de la saisie d’un contact il faut
obligatoirement le nom, prénom et au moins un numéro de téléphone. Le type contact doit être implémenté dans combobox (boite permettant de choisir dans une liste déroulante)
— Recherche un contact par nom, prénom ou numéro de téléphone
— Afficher le nombre de contacts dans la mémoire

3 La partie obligatoire

(donc pour avoir plus de 10).
— Votre projet doit être obligatoirement codé en C avec la librairie GTK+. — Utiliser GLib au lieu de glibc à chaque fois que c’est possible.
— Interface graphique obligatoire avec GTK.
— Implémenter les fonctionnalités décrites ci-dessus.

4 Bonus

Les bonus ci-dessous peuvent apporter des points s’ils sont bien réalisés et si le reste fonctionne. Ils ne sont pas inclus dans la notation (donc pas nécessaire pour avoir 20).
— Nombre de numéros de téléphone illimité par contact.
— Sauvegarder/ charger les contacts dans un fichier.
— Ajouter la photo du contact.
— Attention : faire un bonus de mauvaise qualité entraînera des pénalités. — L’utilisation d’anjuta n’est pas obligatoire.

5 Le rapport
— Rédiger en LATEX.
— Entre 5 et 10 pages.
— Décrire comment vous avez implémenter votre application.


6 Consignes générales
— Le projet est à réaliser en binôme pas de trinôme.
— Le projet est à rendre sur Moodle au plus tard lundi 19 décembre 2020 à minuit.
