#include <stdlib.h>
#include <gtk/gtk.h>
 
 /* Fonctions callbacks associées aux signaux */

 void DeleteWindow(GtkWidget *Widget, gpointer Data);

 
int main(int argc, char **argv)
{
    /* Déclaration du widget */
    GtkWidget * MainWindow = NULL;
    gtk_init(&argc, &argv);
    /* Création d'une fenêtre */
    MainWindow = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title(GTK_WINDOW(MainWindow), "Contact Book");
    gtk_window_set_default_size(GTK_WINDOW(MainWindow), 1000, 600); 
    /* Connexion du signal "delete-win" */
    g_signal_connect(G_OBJECT(MainWindow), "delete-win", G_CALLBACK(DeleteWindow), NULL);
    /* Affichage de la fenêtre */
    gtk_widget_show(MainWindow);
    /* lancement de la boucle évènementielle */
    gtk_main();
    /* Fermeture de GTK+ */
    gtk_exit(EXIT_SUCCESS);
    return EXIT_SUCCESS;
}

void DeleteWindow(GtkWidget *Widget, gpointer Data)
{
    /* Arret de la boucle évènementielle */
    gtk_main_quit();
}